type Gender = 'male' | 'female' | 'Other'
type Roles = 'admin' | 'user'
type User = {
  id: number
  email: string
  password: string
  fullName: string
  gender: Gender //Male, Female, Others
  roles: Roles[] // admin, user
}

export type {Gender, Roles, User}